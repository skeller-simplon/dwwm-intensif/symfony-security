<?php

namespace App\Example;

interface FirstInterface{
    function doStuff(int $param):string;
    function useInterface(FirstInterface $firstInterface);

}