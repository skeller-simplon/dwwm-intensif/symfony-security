<?php

namespace App\Example;

class FirstImplementation implements FirstInterface
{
    public function doStuff(int $param): string
    {
        return " " . $param;
    }
    public function useInterface(FirstInterface $firstInterface){
        echo $firstInterface->doStuff(23);
    }
}

