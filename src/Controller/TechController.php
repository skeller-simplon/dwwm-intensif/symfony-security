<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Tech;
use App\Form\TechType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;

class TechController extends AbstractController
{
    /**
     * @Route("/profile/add-tech", name="add_tech")
     */
    public function addTech(Request $request, ObjectManager $objectManager)
    {
        $tech = new Tech;
        $form = $this->createForm(TechType::class, $tech);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $objectManager->persist($tech);
            $objectManager->flush();
            return $this->redirectToRoute('home');
        }
        return $this->render('tech/add-tech.html.twig', [
            "form" => $form->createView()
        ]);
    }
}
